var searchData=
[
  ['img_5finsg',['IMG_INSG',['../dati_8cc.html#a80eaf3a8aa0fe2d3cc9bb46b26695585',1,'IMG_INSG():&#160;dati.cc'],['../fileIO_8cc.html#a80eaf3a8aa0fe2d3cc9bb46b26695585',1,'IMG_INSG():&#160;dati.cc']]],
  ['img_5fspdc',['IMG_SPDC',['../dati_8cc.html#aa902400e3c3722eb9033b6705e9c4939',1,'IMG_SPDC():&#160;dati.cc'],['../fileIO_8cc.html#aa902400e3c3722eb9033b6705e9c4939',1,'IMG_SPDC():&#160;dati.cc']]],
  ['img_5ftank',['IMG_TANK',['../dati_8cc.html#a2ddadc7056d05ff357aef6eb9ca84494',1,'IMG_TANK():&#160;dati.cc'],['../fileIO_8cc.html#a2ddadc7056d05ff357aef6eb9ca84494',1,'IMG_TANK():&#160;dati.cc']]],
  ['immagine',['immagine',['../structinformazioni__disegno.html#aed29f06a9b9d73d2d595c9d7797089fa',1,'informazioni_disegno']]],
  ['in_5fattacco',['in_attacco',['../structpersonaggio.html#a52fbcf8eca899dd3399be2e19e7c4c0b',1,'personaggio']]],
  ['in_5fmovim_5fdx',['in_movim_dx',['../structpersonaggio.html#a6a3cbc9189be5d105338b465ec49bd35',1,'personaggio']]],
  ['in_5fmovim_5fsx',['in_movim_sx',['../structpersonaggio.html#aa64b25451d1998872aed5403b6298f94',1,'personaggio']]],
  ['in_5fsalto',['in_salto',['../structpersonaggio.html#a9433a0a393b90739abb7c95f39c80771',1,'personaggio']]],
  ['in_5fuscita',['in_uscita',['../structoggetto.html#ab26bec51733d8211e5593cf0eed86f04',1,'oggetto']]],
  ['insg_5fhp',['INSG_HP',['../dati_8h.html#aeffec56f717e7b0a18b67d01742eadb3',1,'dati.h']]],
  ['invio',['invio',['../eventi_8h.html#a565ba9c9472c8826640e0be949d2ed3a',1,'eventi.h']]]
];
