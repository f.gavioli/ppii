var searchData=
[
  ['padre',['padre',['../structoggetto.html#a19939efcb5676eef1b4d95fd1c777ec4',1,'oggetto']]],
  ['pausa',['PAUSA',['../dati_8cc.html#a35d38ebf668bbfa1c86974f543a35f14',1,'PAUSA():&#160;dati.cc'],['../eventi_8cc.html#a35d38ebf668bbfa1c86974f543a35f14',1,'PAUSA():&#160;dati.cc'],['../fileIO_8cc.html#a35d38ebf668bbfa1c86974f543a35f14',1,'PAUSA():&#160;dati.cc'],['../main_8cc.html#a35d38ebf668bbfa1c86974f543a35f14',1,'PAUSA():&#160;dati.cc']]],
  ['personaggi',['personaggi',['../structdati__gioco.html#a12e014110bcaf81336ff993a67b292f2',1,'dati_gioco']]],
  ['pg_5fhp',['PG_HP',['../dati_8h.html#ab5d86499bdd0d1af24eda3a895ce53f2',1,'dati.h']]],
  ['pg_5fogg',['PG_OGG',['../dati_8h.html#abaabfd8ec59fad4e4d5a85c70e13f0a4',1,'dati.h']]],
  ['pg_5fsprite',['PG_SPRITE',['../dati_8cc.html#a5e156b89fe62549bbe2c63f2cedb86cb',1,'PG_SPRITE():&#160;dati.cc'],['../fileIO_8cc.html#a5e156b89fe62549bbe2c63f2cedb86cb',1,'PG_SPRITE():&#160;dati.cc'],['../main_8cc.html#a5e156b89fe62549bbe2c63f2cedb86cb',1,'PG_SPRITE():&#160;dati.cc']]],
  ['pt_5fper_5fdan_5fcmp',['PT_PER_DAN_CMP',['../statistiche_8h.html#a2a1ee7f47e1e83dd72420064092dd4d1',1,'statistiche.h']]],
  ['pt_5fper_5fdan_5fogg',['PT_PER_DAN_OGG',['../statistiche_8h.html#a080f3bba812871943624869e72b6eba9',1,'statistiche.h']]],
  ['pt_5fper_5fdan_5fspd',['PT_PER_DAN_SPD',['../statistiche_8h.html#a8175e354373623d9ea06872b170622ff',1,'statistiche.h']]],
  ['pt_5fper_5felm',['PT_PER_ELM',['../statistiche_8h.html#ab699c5418751c7e07d456b0b41d81340',1,'statistiche.h']]],
  ['pt_5fper_5fsec',['PT_PER_SEC',['../statistiche_8h.html#a93ac190ed55154d83c882373e6059bc0',1,'statistiche.h']]],
  ['punteggio',['punteggio',['../structstatistiche.html#abff9b472410ee57cb919d991957b38a7',1,'statistiche::punteggio()'],['../structhighscore.html#a18b417e85925aa34816f7aad1b8770e8',1,'highscore::punteggio()']]]
];
