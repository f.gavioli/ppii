var searchData=
[
  ['random_5ftipo',['random_tipo',['../dati_8cc.html#a1aa08b2f49dbe84af9b36379089b758a',1,'dati.cc']]],
  ['range_5fatt_5fspdc',['RANGE_ATT_SPDC',['../ia_8h.html#a73e29f69ac8c6e72a4b13b8c7bc263e3',1,'ia.h']]],
  ['range_5fatt_5ftank',['RANGE_ATT_TANK',['../ia_8h.html#a134180343c9aeb1c304fc57ac7b6b895',1,'ia.h']]],
  ['range_5fsalto_5finsg',['RANGE_SALTO_INSG',['../ia_8h.html#a40d63df49bc74759b2b4b9bdda359167',1,'ia.h']]],
  ['range_5fvisione',['RANGE_VISIONE',['../ia_8h.html#ac1130ac558d8c61a451f2e16686a7a1f',1,'ia.h']]],
  ['registra_5fsorgenti_5feventi',['registra_sorgenti_eventi',['../eventi_8cc.html#a4f1891421f203be076bdfbff21674b77',1,'registra_sorgenti_eventi(dati_eventi &amp;eventi, dati_schermo &amp;schermo):&#160;eventi.cc'],['../eventi_8h.html#a4f1891421f203be076bdfbff21674b77',1,'registra_sorgenti_eventi(dati_eventi &amp;eventi, dati_schermo &amp;schermo):&#160;eventi.cc']]],
  ['rim',['rim',['../structoggetto.html#a5585997dcfbdf5bf957f501b8a8b1fc2',1,'oggetto::rim()'],['../structpersonaggio.html#a7b6dc2121c5bf513b38e0820a91a605f',1,'personaggio::rim()']]],
  ['risolvi_5fcollisioni_5fogg_5fogg',['risolvi_collisioni_ogg_ogg',['../dati_8cc.html#a06dce035668c284dfe5b39b28c809a1b',1,'risolvi_collisioni_ogg_ogg(list&lt; oggetto &gt; &amp;oggetti):&#160;dati.cc'],['../dati_8h.html#a06dce035668c284dfe5b39b28c809a1b',1,'risolvi_collisioni_ogg_ogg(list&lt; oggetto &gt; &amp;oggetti):&#160;dati.cc']]],
  ['risolvi_5fcollisioni_5fogg_5fpg',['risolvi_collisioni_ogg_pg',['../dati_8cc.html#a0a88e7a36178ae8d518c0a639686ef12',1,'risolvi_collisioni_ogg_pg(list&lt; oggetto &gt; &amp;oggetti, list&lt; personaggio &gt; &amp;personaggi):&#160;dati.cc'],['../dati_8h.html#a0a88e7a36178ae8d518c0a639686ef12',1,'risolvi_collisioni_ogg_pg(list&lt; oggetto &gt; &amp;oggetti, list&lt; personaggio &gt; &amp;personaggi):&#160;dati.cc']]],
  ['risolvi_5fcollisioni_5fpg_5fpg',['risolvi_collisioni_pg_pg',['../dati_8cc.html#a43636642d67e03810fb270bc33e2c7ca',1,'risolvi_collisioni_pg_pg(list&lt; personaggio &gt; &amp;personaggi):&#160;dati.cc'],['../dati_8h.html#a43636642d67e03810fb270bc33e2c7ca',1,'risolvi_collisioni_pg_pg(list&lt; personaggio &gt; &amp;personaggi):&#160;dati.cc']]],
  ['rotazione',['rotazione',['../structinformazioni__disegno.html#a759242a4c613f0d61c9cd8a8e9011b3f',1,'informazioni_disegno']]],
  ['ruota_5fvisuale',['ruota_visuale',['../ia_8cc.html#a8628e7379785b196873b13eda30520f7',1,'ia.cc']]]
];
