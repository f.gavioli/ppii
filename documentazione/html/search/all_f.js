var searchData=
[
  ['tank',['TANK',['../dati_8h.html#a9bb20167eb424d00ab6fae7c495cd2e7ac421798d7f033c92c22ec2531e41d1cb',1,'dati.h']]],
  ['tank_5fhp',['TANK_HP',['../dati_8h.html#aa7ca137df186e961c6001f54f169e7cb',1,'dati.h']]],
  ['timer_5fagg',['timer_agg',['../structdati__eventi.html#a146cd3670533b3411bf0bfdbc17b10f5',1,'dati_eventi']]],
  ['timer_5fanim',['timer_anim',['../structdati__eventi.html#a30782f46f0acce84064e84c5654952e6',1,'dati_eventi']]],
  ['timer_5ffps',['timer_fps',['../structdati__eventi.html#a6ef18830de55ab26bbf42f7897f694b5',1,'dati_eventi']]],
  ['timer_5fgen_5farmi_5fpg',['timer_gen_armi_pg',['../structdati__eventi.html#a84fb137c7ccafae4972718bdce9f0b7a',1,'dati_eventi']]],
  ['timer_5fgen_5farmi_5fpng',['timer_gen_armi_png',['../structdati__eventi.html#a76d7e584e4f1c60f3e97a4eca6f23b52',1,'dati_eventi']]],
  ['timer_5fgen_5fnemici',['timer_gen_nemici',['../structdati__eventi.html#abef7e931cafa31ac60712629e08a621f',1,'dati_eventi']]],
  ['timer_5fia',['timer_ia',['../structdati__eventi.html#a84fc494d1dd70221c24f7166c18651af',1,'dati_eventi']]],
  ['timer_5fmov_5fogg',['timer_mov_ogg',['../structdati__eventi.html#a7c5ecc7bb0c6ec0121b75e7d40ef89ee',1,'dati_eventi']]],
  ['timer_5fmov_5fpers',['timer_mov_pers',['../structdati__eventi.html#a7bcddb587acdb98de2ff67ad2b5f8bb4',1,'dati_eventi']]],
  ['timer_5fsalto',['timer_salto',['../structdati__eventi.html#af5bccbde398bb6b4d3e9cb74ae436566',1,'dati_eventi']]],
  ['tipo',['tipo',['../structpersonaggio.html#ada6c8bcef11e0095406b3bdc5ff6c583',1,'personaggio']]],
  ['tipo_5ft',['tipo_t',['../dati_8h.html#a9bb20167eb424d00ab6fae7c495cd2e7',1,'dati.h']]]
];
