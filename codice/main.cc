#include "init.h"
#include "dati.h"
#include "eventi.h"
#include "grafica.h"
#include "ia.h"
#include <iostream>
#include <cstdio>

/*! \mainpage Progetto Programmazione II - Gavioli Federico
 *
 * \section sez_intro Introduzione
 *
 * Questo documento è molto suus
 *
 * \section sez_prog Il progetto
 *
 * Il progetto consiste in un videogioco per sistemi operativi Linux,
 * realizzato con le librerie grafiche Allegro. Il gioco si basa sullo
 * scorrimento orizzontale, prendendo elementi da giochi come Super Mario
 * o Nidhogg, ma rivisitandoli e semplificandoli per adattarli ad un progetto 
 * universitario.
 *
 * \section sez_moduli I moduli
 *
 * La dimensione del progetto non ha permesso lo sviluppo su un solo file,
 * quindi il codice sarà diviso nei diversi moduli (con rispettivi file).
 *
 * \subsection mod_main - Modulo principale
 * Files: main.cc
 * 
 * Il modulo principale dell'applicazione, coordina tutti
 * gli altri moduli sottostanti.
 *
 * \subsection mod_init - Gestione inizializzazioni Allegro
 * Files: init.cc - init.h
 * 
 * Questo modulo si occupa di caricare gli "Addon" necessari
 * al funzionamento delle librerie Allegro.
 *
 * \subsection mod_io - Gestione I/O su file
 * Files: fileIO.cc - fileIO.h
 * 
 * Questo modulo si occupa di caricare tutti i file necessari
 * al funzionamento grafico e di statistica del gioco. Questo
 * comprende: immagini varie di gioco, file dei punteggi.
 *
 * \subsection mod_dati - Gestione dati di funzionamento
 * Files: dati.cc - dati.h
 * 
 * Questo modulo si occupa di eseguire operazioni sui dati
 * di gioco più importanti, che comprende: inizializzazione,
 * generazione di nemici e oggetti, collision detection e 
 * logica applicativa.
 *
 * \subsection mod_schermo - Gestione interfaccia grafica
 * Files: grafica.cc - grafica.h
 * 
 * Questo modulo si occupa di gestire diverse stampe e 
 * disegni a schermo, dall'introduzione, alla schermata
 * di gioco, ai punteggi migliori, alla lettura del nome
 * per la registrazione del punteggio. Il modulo è in stretto
 * legame con dati.h, il modulo da cui vengono estratte le
 * informazioni da mandare in stampa. 
 *
 * \subsection mod_eventi - Gestione eventi di gioco e timer
 * Files: eventi.cc - eventi.h
 * 
 * Questo modulo si occupa di registrare le sorgenti di ogni
 * evento (timer e tastiera) e gestire la modalità di pausa.
 * Lo smistamento degli eventi con chiamate alle funzioni 
 * interessate è delegato al modulo main.cc.
 *
 * \subsection mod_movimento - Gestione movimento
 * Files: movimento.cc - movimento.h
 * 
 * Questo modulo si occupa di gestire il movimento, che 
 * comprende: aggiornamento velocità di movimento, avanzamento
 * sull'asse x (movimento) e sull'asse y (salto). In questo 
 * modulo si sfruttano funzioni di "collision detection", che  
 * vengono richiamate direttamente da dati.h.
 *
 * \subsection mod_ia - Gestione Intelligenza Artificiale (Termine non troppo preciso ;D)
 * Files: ia.cc - ia.h
 * 
 * Anche se non si può parlare di una vera intelligenza artificiale,
 * ma più di una serie di direttive, questo modulo si occupa di
 * decidere, a volte aggiungendo il fattore casuale per variegare
 * il gioco sul lungo termine, le azioni di ogni personaggio.
 * Ogni personaggio è caratterizzato, ed ha un suo set di mosse
 * personalizzato.
 *
 * \subsection mod_statistiche - Gestione punteggi e statistiche
 * Files: statistiche.cc - statistiche.h
 * 
 * Questo modulo si occupa di gestire le strutture dati necessarie
 * per la registrazione delle statistiche di gioco, e successivamente,
 * del calcolo dei punteggi finali della partita.
 *
 * \section sez_scopo Scopo del gioco
 *
 * Lo scopo del gioco è sopravvivere per il maggior tempo possibile,
 * cercando di eliminare tutti i nemici.
 *
 * \section sez_comandi Comandi
 *
 * A - D Movimento a Sinistra - Destra
 * 
 * J - K Attacco ravvicinato - a distanza
 * 
 * SPAZIO Salto
 *
 * INVIO Pausa
 * 
 * ESC Interrompi la partita
 *
 * \section sez_nemici Tipi di nemici
 *
 * Inseguitore
 * Insegue il personaggio, e quando arriva a gittata, ci salta sopra.
 * Si muove molto velocemente, ma non ha armi.
 *
 * Spadaccino
 * Corre verso il personaggio, e quando arriva a gittata, cerca di colpirlo con la sua spada.
 * Si muove alla velocità del personaggio.
 * 
 * Tank
 * Si muove verso il personaggio, quando arriva a gittata cerca di colpirlo con la sua spada.
 * Infligge il doppio del danno, e possiede il doppio della vita, ma è molto lento.
 *
 * \section sez_punt Punteggio
 * Il punteggio è calcolato in base ai danni ricevuti, al numero di nemici colpiti, e alla
 * durata della partita.
 *
 *
 */

using namespace std;

//#define DBG 1

const int FPS               =  144;         /**< Frequenza di aggiornamento dello schermo*/
const int VEL_SALTO         =  110;         /**< Velocità di salto*/
const int MOV_OGG_PS        =   60;         /**< Frequenza di movimento oggetti*/
const int MOV_PERS_PS       =   50;         /**< Frequenza di movimento personaggi*/
const int ACT_IA_PS         =   15;         /**< Azioni di Intelligenza Artificiale per secondo*/
const int AGGPS             =   10;         /**< Frequenza di accelerazione personaggi*/
const int GEN_ARMI_PG_PM    =   10;         /**< Frequenza di generazione armi personaggio al minuto*/
const int GEN_ARMI_PNG_PM   =   15;         /**< Frequenza di generazione armi nemici al minuto*/
const int GEN_NEM_PM        =   30;         /**< Frequenza di generazione nemici*/
const int ANIM_FPS          =    4;         /**< Frequenza di avanzamento animazioni (NON IMPLEMENTATO)*/

const int MAX_NEMICI        =    5;         /**< Numero massimo di nemici che possono essere generati*/

const char *FONT_PATH       =   "../media/font/hud.ttf";
const int FONT_SIZE         =   50;

extern ALLEGRO_BITMAP *PG_SPRITE;
extern ALLEGRO_BITMAP *COL_SX;
extern ALLEGRO_BITMAP *COL_DX;
extern ALLEGRO_BITMAP *LAN_SX;
extern ALLEGRO_BITMAP *LAN_DX;
extern ALLEGRO_BITMAP *SFONDO_1;
extern ALLEGRO_BITMAP *COL_HUD;
extern ALLEGRO_BITMAP *HP_HUD;
extern ALLEGRO_BITMAP *NPC_HUD;
extern ALLEGRO_BITMAP *PAUSA;
extern ALLEGRO_BITMAP *DIR_DX;
extern ALLEGRO_BITMAP *DIR_SX;

extern statistiche stat;

/** Funzione di cleanup della memoria, che libera tutte le aree
  * di memoria dinamica occupate precedentemente
 */
void pulisci_memoria(dati_gioco &gioco, dati_schermo &schermo, dati_eventi &eventi)
{
    al_destroy_event_queue(eventi.coda_eventi);
    al_destroy_timer(eventi.timer_fps);
    al_destroy_timer(eventi.timer_anim);
    al_destroy_timer(eventi.timer_mov_pers);
    al_destroy_timer(eventi.timer_mov_ogg);
    al_destroy_timer(eventi.timer_agg);
    al_destroy_timer(eventi.timer_salto);
    al_destroy_timer(eventi.timer_gen_nemici);
    al_destroy_timer(eventi.timer_gen_armi_pg);
    al_destroy_timer(eventi.timer_gen_armi_png);
    al_destroy_timer(eventi.timer_ia);
    al_destroy_bitmap(PG_SPRITE);
    al_destroy_bitmap(COL_SX);
    al_destroy_bitmap(COL_DX);
    al_destroy_bitmap(LAN_SX);
    al_destroy_bitmap(LAN_DX);
    al_destroy_bitmap(COL_HUD);
    al_destroy_bitmap(HP_HUD);
    al_destroy_bitmap(NPC_HUD);
    al_destroy_display(schermo.display);
}
/** Funzione che stampa a schermo l'immagine 
  * introduttiva del gioco, che spiega comandi,
  * tipi di nemici e condizioni di gioco.
 */
bool introduzione(dati_schermo schermo)
{
    ALLEGRO_BITMAP *intro = al_load_bitmap("../media/img/intro.png");
    al_clear_to_color(al_map_rgb(0,0,0));
    al_draw_bitmap(intro, 0, 0, 0);
    al_flip_display();

    ALLEGRO_EVENT_QUEUE *q = al_create_event_queue();
    ALLEGRO_EVENT *evt = new ALLEGRO_EVENT;
    al_register_event_source(q, al_get_keyboard_event_source());
    al_register_event_source(q, al_get_display_event_source(schermo.display));
    
    do
    {
        al_wait_for_event(q, evt);
        if(evt->keyboard.keycode == ALLEGRO_KEY_ESCAPE || evt->type == ALLEGRO_EVENT_DISPLAY_CLOSE)
            return true;
    }while (evt->keyboard.keycode != ALLEGRO_KEY_SPACE);

    al_unregister_event_source(q, al_get_keyboard_event_source());
    al_destroy_event_queue(q);
    return false;
}

/** Funzione che accende i timer, dando inizio
 * al gioco.
 */
bool inizia_gioco(dati_eventi &eventi)
{
    if(!eventi.timer_fps)
        return false;
    if(!eventi.timer_anim)
        return false;
    if(!eventi.timer_mov_pers)
        return false;
    if(!eventi.timer_mov_ogg)
        return false;
    if(!eventi.timer_agg)
        return false;
    if(!eventi.timer_salto)
        return false;
    if(!eventi.timer_gen_nemici)
        return false;
    if(!eventi.timer_gen_armi_pg)
        return false;
    if(!eventi.timer_gen_armi_png)
        return false;
    start_timers(eventi);
    return true;
}

/*FUNZIONE MAIN*/
int main()
{
    dati_gioco dati;
    dati_schermo schermo;
    dati_eventi eventi;

    if(!inizializza_addon())
    {
    	cout << "Errore nell'inizializzazione delle librerie Allegro." << endl;
        return 1;
    }
    #ifdef DBG
        cout << "Load Immagini." << endl;
    #endif
    if(!load_immagini("../media/img/sfondo.png", "../media/img/pg.png", "../media/img/coltello_dx.png",
                    "../media/img/coltello_sx.png", "../media/img/lancia_dx.png", "../media/img/lancia_sx.png", 
                    "../media/img/spada_dx.png", "../media/img/spada_sx.png", "../media/img/hp_hud.png", "../media/img/col_hud.png",
                    "../media/img/npc_hud.png", "../media/img/pausa.png", "../media/img/dir_dx.png", "../media/img/dir_sx.png",
                    "../media/img/spadaccino.png", "../media/img/tank.png", "../media/img/inseguitore.png"))
    {
        cout << "Immagini non caricate correttamente" << endl;
        return 1;
    }
    
    if(!inizializza_schermo(schermo, 1366, 768, FPS, SFONDO_1, COL_HUD, HP_HUD, NPC_HUD, FONT_PATH, FONT_SIZE))
    {
        cout << "Errore nell'inizializzazione della struttura schermo." << endl;
        pulisci_memoria(dati, schermo, eventi);
        return 1;
    }
    
    eventi = inizializza_dati_eventi(FPS, ANIM_FPS, MOV_PERS_PS, AGGPS, MOV_OGG_PS, VEL_SALTO, GEN_NEM_PM, 
                                        GEN_ARMI_PG_PM, GEN_ARMI_PNG_PM, ACT_IA_PS);
    
    bool exit = introduzione(schermo);
    stampa_highscores(NULL);

    #ifdef DBG
    cout << "Load Personaggio" << endl;
    #endif
    
    srand(time(0));
    
    personaggio p = inizializza_personaggio(100, Y_TERRENO - ALT_SPRITE, 0, PRINCIPALE, DX);
    dati.personaggi.push_front(p);

    stat = inizializza_statistiche();

    #ifdef DBG
    cout << "Registrazione sorgenti eventi" << endl;
    #endif
    registra_sorgenti_eventi(eventi, schermo);
    ALLEGRO_EVENT *evento_attuale = new ALLEGRO_EVENT;    
    
    #ifdef DBG
	cout << "Partenza timer" << endl;
    #endif
    if(!inizia_gioco(eventi))
    {
        cout << "Problema relativo ai timer Allegro." << endl;
        pulisci_memoria(dati, schermo, eventi);
        return 1;
    }
    #ifdef DBG
    cout << "Inizio Game Loop" << endl;
	#endif

    bool pausa = false;

    while(!exit)
    {
        al_wait_for_event(eventi.coda_eventi, evento_attuale);
    
        switch(evento_attuale->type)
		{
			case ALLEGRO_EVENT_DISPLAY_CLOSE:
				exit = true;
                break;

            case ALLEGRO_EVENT_DISPLAY_SWITCH_OUT:
                if(!pausa)
                    in_pausa(eventi, pausa);

			case ALLEGRO_EVENT_KEY_DOWN:
                exit = gestore_tasto_giu(eventi, evento_attuale, dati, pausa);
				
                break;

			case ALLEGRO_EVENT_KEY_UP:
                    gestore_tasto_su(eventi, evento_attuale, dati, pausa);
				
                break;

			case ALLEGRO_EVENT_TIMER:
                if(evento_attuale->timer.source == eventi.timer_fps)
                {
                    stampa_fotogramma(schermo, dati);
                    stat.durata_partita += 1.0/FPS;
                    schermo.serve_disegno = false;
                }
                /*else if(evento_attuale->timer.source == eventi.timer_anim)
                {    
                    avanza_animazioni(schermo, dati);
                    schermo.serve_disegno = true;
                }*/
                else if(evento_attuale->timer.source == eventi.timer_mov_pers)
                {
                    exit = muovi_personaggi(schermo, dati.personaggi, dati.oggetti);
                    schermo.serve_disegno = true;
                }
                else if(evento_attuale->timer.source == eventi.timer_salto)
                {
                    exit = salta(schermo, dati.oggetti, dati.personaggi);
                    schermo.serve_disegno = true;
                }
                else if(evento_attuale->timer.source == eventi.timer_mov_ogg)
                {
                    exit = muovi_oggetti(schermo, dati.oggetti, dati.personaggi);
                    schermo.serve_disegno = true;
                }
                else if(evento_attuale->timer.source == eventi.timer_agg)
                {
                    aggiorna_velocita_x(dati);
                }
                else if(evento_attuale->timer.source == eventi.timer_gen_nemici)
                {
                    if(dati.personaggi.size() - 1 <= MAX_NEMICI)   
                    {
                        genera_png(dati, Y_TERRENO);
                        schermo.serve_disegno = true;
                        #ifdef DBG
                        cout << "Generato nemico con personalità " << dati.personaggi.back().tipo << endl;
                        #endif
                    }
                    #ifdef DBG
                    else
                        cout << "Impossibile generare altri nemici, numero massimo di nemici su schermo raggiunto" << endl;
                    #endif
                }
                else if(evento_attuale->timer.source == eventi.timer_gen_armi_pg)
                {
                    personaggio &p = dati.personaggi.front();
                    if(p.num_oggetti + 1 < PG_OGG)
                        p.num_oggetti++;
                }
                /*else if(evento_attuale->timer.source == eventi.timer_gen_armi_png)
                {
                    persona &p = dati.personaggi.effetti_front();
                    if(p.num_oggetti + 1 < NPC_OGG)
                        p.num_oggetti++;
                }*/
                /*
                 * Stesso scopo di tasto down/up, ma per i nemici
                */
                else if(evento_attuale->timer.source == eventi.timer_ia)
                {
                    /*

                    PRIMA VERSIONE IA, PARZIALMENTE IMPLEMENTATA E ABBANDONATA

                    //Inizializzo un array di puntatori a personaggio. 
                    personaggio *pgs[MAX_NEMICI];
                    int dim = 0;
                    //Trovo i nemici abili a fare un'azione
                    for(personaggio &p : dati.personaggi)
                    {
                        if(p.tipo == SPADACCINO)
                            ia_spadaccino();
                        //else if(p.tipo == LANCIERE && p.num_oggetti != 0 && dati.oggetti.size() < 3)
                        //{
                          ///  pgs[dim] = &p;
                            //dim++;
                      //  }//else if(p.tipo == INSEGUITORE)
                    }
                    
                    //Se c'è almeno un riferimento nell'array pgs allora scelgo un nemico da far agire (random) 
                    if(dim > 0)
                    {
                        int scelto = rand() % dim;
                        esegui_azione(dati, *pgs[scelto]);
                    }

                    
                    */

                    for(personaggio &p : dati.personaggi)
                    {
                        if(p.tipo == SPADACCINO)
                            ia_spadaccino(dati, p);
                        else if(p.tipo == INSEGUITORE)
                            ia_inseguitore(dati, p);
                        else if(p.tipo == TANK)
                            ia_tank(dati, p);
                    }

                }

                break;

            default:
                break;
        }
    }

    #ifdef DBG
    cout << "Fine gioco" << endl;
    #endif

    if(!((int)stat.durata_partita == 0))
    {
        char *nome;
        calcola_punteggio(stat);
	    stampa_risultati(schermo);
        nome = leggi_nome(schermo);

        highscore h;
        h.nome[0] = nome[0];
        h.nome[1] = nome[1];
        h.nome[2] = nome[2];
        
        h.punteggio = stat.punteggio;
        
        delete[] nome;
        salva_risultati(h);
        stampa_highscores(&h);
    }

    delete evento_attuale;
    pulisci_memoria(dati, schermo, eventi);
	
    return 0;
}