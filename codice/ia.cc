#include "ia.h"

extern ALLEGRO_BITMAP *DIR_DX;
extern ALLEGRO_BITMAP *DIR_SX;

bool esiste_arma_vicina(list<oggetto> oggetti, personaggio p)
{
    for(oggetto o : oggetti)
    {
        if(abs(p.x - o.x) < RANGE_VISIONE + (p.tipo == SPADACCINO)*40)
            return true;
    }
    return false;
}

void muovi_png(personaggio &p)
{
    int moltiplicatore = 1;
    if(p.tipo == SPADACCINO)
        moltiplicatore = 2;
    else if (p.tipo == INSEGUITORE)
        moltiplicatore = 4;
    if(p.direzione == DX)
        p.vel_x = (MAX_VEL_X/3)*moltiplicatore;
    else
        p.vel_x = (MIN_VEL_X/3)*moltiplicatore; 
}

void ruota_visuale(personaggio &p, personaggio pg)
{
    if(p.x < pg.x)
    {
        p.direzione = DX;
        p.dir_img = DIR_DX;
    }
    else
    {
        p.direzione = SX;
        p.dir_img = DIR_SX;
    }
}

void ia_spadaccino(dati_gioco &dati, personaggio &p)
{
    if(p.cade || p.in_salto)
        return ;
    /*
        Verifico se c'è un'arma da lancio vicino a lui, e nel
        caso fosse vero, lo faccio saltare al 40% di possibilità.
    */
    if(esiste_arma_vicina(dati.oggetti, p))
    {
        // 50% di possibilità di salto
        if((rand() % 2))
        {
            p.in_salto = true;
            return ;
        }
    }

    ruota_visuale(p, dati.personaggi.front());

    if(abs(p.x - dati.personaggi.front().x) < RANGE_ATT_SPDC)
    {
        p.vel_x = 0;
        attacca(p);
    }
    else
        muovi_png(p);
    return ;
}

void ia_inseguitore(dati_gioco &dati, personaggio &p)
{
    // Verifico che il personaggio non sia in salto
    if(p.cade || p.in_salto)
        return ;
    /*
        Verifico se c'è un'arma da lancio vicino a lui, e nel
        caso fosse vero, lo faccio saltare al 40% di possibilità.
    */
    if(esiste_arma_vicina(dati.oggetti, p))
    {
        // 70% di possibilità di salto
        if((rand() % 10) < 6)
        {
            p.in_salto = true;
            return ;
        }
    }
    
    ruota_visuale(p, dati.personaggi.front());

    if(abs(p.x - dati.personaggi.front().x) < RANGE_SALTO_INSG)
        p.in_salto = true;
    muovi_png(p);
    return ;
}

void ia_tank(dati_gioco &dati, personaggio &p)
{

    if(abs(p.x - dati.personaggi.front().x) < RANGE_ATT_TANK)
    {
        p.vel_x = 0;
        attacca(p);
        return;
    }
    ruota_visuale(p, dati.personaggi.front());
    muovi_png(p);
}