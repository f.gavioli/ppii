#include "grafica.h"

extern statistiche stat;

bool inizializza_schermo(dati_schermo &schermo, int larg_disegno, int alt_disegno, int fps, ALLEGRO_BITMAP *img_sfondo,
                            ALLEGRO_BITMAP* img_hp_hud, ALLEGRO_BITMAP* img_col_hud, ALLEGRO_BITMAP* img_npc_hud,
                            const char *hud_path, int hud_font_size)
{
    //Setup del monitor (controllo se c'e' un monitor...)
    if(al_get_num_video_adapters() == 0)
        return false;

    al_set_new_display_flags(ALLEGRO_FULLSCREEN_WINDOW);
    schermo.display = al_create_display(larg_disegno, alt_disegno);
    schermo.sfondo = img_sfondo;
    schermo.nvite = img_hp_hud;
    schermo.narmi = img_col_hud;
    schermo.nnpc = img_npc_hud;
    schermo.larghezza = larg_disegno;
    schermo.altezza = alt_disegno;
    schermo.serve_disegno = true;
    schermo.fps = fps;
    schermo.hud_font = al_load_ttf_font(hud_path, hud_font_size, 0);

    return true;
}

void avanza_animazioni(dati_schermo &schermo, dati_gioco &gioco)
{
    
}

void stampa_sfondo(dati_schermo &schermo)
{
    al_draw_bitmap(schermo.sfondo, 0, 0, 0);
}

void stampa_strutture(const list<struttura> strutture)
{

}

void stampa_effetti_retro(const list<effetto> effetti_retro)
{
    
}

void stampa_oggetti(const list<oggetto> oggetti)
{ 
    for(oggetto o : oggetti)
        if(o.sprite.visibile)
            al_draw_bitmap(o.sprite.immagine, o.x, o.y, 0);

}

void stampa_personaggi(const list<personaggio> personaggi)
{
    for(personaggio p : personaggi)
        if(p.sprite.visibile)
        {
            //Disegno arma
            if(p.arma.sprite.visibile)
                al_draw_bitmap(p.arma.sprite.immagine, p.arma.x, p.arma.y, 0);
            //Disegno personaggio
            al_draw_bitmap(p.sprite.immagine, p.x, p.y, 0);
            //Disegno freccia direzionale
            if(p.direzione == DX)
                al_draw_bitmap(p.dir_img, p.x + LARG_SPRITE - OFFSET_IMG_DIR, p.y + OFFSET_ALT_MELEE, 0);
            else
                al_draw_bitmap(p.dir_img, p.x + OFFSET_IMG_DIR - LARG_IMG_DIR, p.y + OFFSET_ALT_MELEE, 0);
        }
}

void stampa_effetti_fronte(const list<effetto> effetti_fronte)
{
    
}

void stampa_interfaccia_dati(const dati_gioco &dati, const dati_schermo &schermo)
{
    
    // Trasformazione dei dati dell'hud da interi ad array di caratteri
    char vita[5];
    sprintf(vita, "x%d", dati.personaggi.front().hp);

    char armi[5];
    sprintf(armi, "x%d", dati.personaggi.front().num_oggetti);
    
    char npc[5];
    sprintf(npc, "x%d", stat.nemici_eliminati);
    

    //Disegno l'immagine dell'hud e successivamente la sua stringa
    al_draw_bitmap (schermo.nvite, X_HUD, Y_OFFSET_HUD, 0);
    al_draw_bitmap (schermo.narmi, X_HUD, Y_INTERLINEA + Y_OFFSET_HUD, 0);
    al_draw_bitmap (schermo.nnpc, X_HUD, Y_INTERLINEA*2 + Y_OFFSET_HUD, 0);
    
    al_draw_text(schermo.hud_font, al_map_rgb(255, 255, 255), X_TESTO_HUD,  
                    Y_OFFSET_HUD, ALLEGRO_ALIGN_LEFT, vita);
    al_draw_text(schermo.hud_font, al_map_rgb(255, 255, 255), X_TESTO_HUD, 
                    Y_INTERLINEA + Y_OFFSET_HUD, ALLEGRO_ALIGN_LEFT, armi);
    al_draw_text(schermo.hud_font, al_map_rgb(255, 255, 255), X_TESTO_HUD, 
                    Y_INTERLINEA*2 + Y_OFFSET_HUD, ALLEGRO_ALIGN_LEFT, npc);

}

void stampa_risultati_hud(highscore h)
{
    
}

void stampa_fotogramma(dati_schermo &schermo, const dati_gioco &dati)
{
    al_clear_to_color(al_map_rgb(0,0,0));
    stampa_sfondo(schermo);
    //stampa_strutture(dati.strutture);
    //stampa_effetti_retro(dati.effetti_retro);
    stampa_oggetti(dati.oggetti);
    stampa_personaggi(dati.personaggi);
    //stampa_effetti_fronte(dati.effetti_fronte);
    stampa_interfaccia_dati(dati, schermo);
    al_flip_display();
}

char* leggi_nome(dati_schermo &schermo)
{
    char *nome = new char[4];
    nome[0] = '_';
    nome[1] = '_';
    nome[2] = '_';
    int i = 0;
    ALLEGRO_FONT* font = al_load_font("../media/font/hud.ttf", 30, 0);

    ALLEGRO_EVENT_QUEUE *q = al_create_event_queue();
    al_register_event_source(q, al_get_keyboard_event_source());
    ALLEGRO_EVENT *evt = new ALLEGRO_EVENT;

    stampa_risultati(schermo);
    al_draw_text(font, al_map_rgb(255, 255, 255), X_TESTO_HUD,
                Y_INTERLINEA*9 + Y_OFFSET_HUD, ALLEGRO_ALIGN_LEFT,
                "Inserisci il nome da visualizzare nella lista dei punteggi migliori: _ _ _");
    al_flip_display();
    do
    {
        al_wait_for_event(q, evt);
        if(((evt->keyboard.unichar >= 'a' 
             && evt->keyboard.unichar <= 'z')
             || evt->keyboard.unichar == 8) && !(evt->keyboard.repeat))
        {
            if(evt->keyboard.keycode == ALLEGRO_KEY_BACKSPACE)
            {
                if(i > 0)
                    i--;
                nome[i] = '_';
            }
            else
            {
                if(i < 3)
                {
                    nome[i] = evt->keyboard.unichar - 32;
                    i++;
                }
            }
            al_clear_to_color(al_map_rgb(0, 0, 0));
            stampa_risultati(schermo);
            char testo[80] = "Inserisci il nome da visualizzare nella lista dei punteggi migliori: %c %c %c";
            char display[100];
            sprintf(display, testo, nome[0], nome[1], nome[2]);
            al_draw_text(font, al_map_rgb(255, 255, 255), X_TESTO_HUD,
                        Y_INTERLINEA*9 + Y_OFFSET_HUD, ALLEGRO_ALIGN_LEFT,
                        display);
            al_flip_display();
        }
    }while (evt->keyboard.keycode != ALLEGRO_KEY_ENTER);

    al_destroy_font(font);

    return nome;
}

void stampa_risultati(dati_schermo schermo)
{

    ALLEGRO_FONT* font = al_load_font("../media/font/hud.ttf", 30, 0);

    //Stampa a schermo
    al_clear_to_color(al_map_rgb(0,0,0));
    char text[][50] = {"Nemici eliminati: %.f", "Oggetti lanciati: %.f",
                    "Durata partita: %.00f",
                    "Danni ricevuti per compenetrazione: %.f", 
                    "Danni ricevuti da armi ravvicinate: %.f",
                    "Punteggio Finale: %.f"};

    for(int i = 0; i < 6; i++)
    {
        char finale[50];
        sprintf(finale, text[i], stats(stat, i));
        al_draw_text(font, al_map_rgb(255, 255, 255), X_TESTO_HUD, 
                     Y_INTERLINEA*i + Y_OFFSET_HUD + (i == 6)*Y_INTERLINEA, ALLEGRO_ALIGN_LEFT,
                     finale);
    }
    
    al_flip_display();
    al_destroy_font(font);
}


void stampa_highscores(highscore *h)
{
    int N;
    highscore *hs = leggi_highscores(N);
    if(N == 0)
        return;
    ALLEGRO_FONT* font = al_load_font("../media/font/monos.ttf", 35, 0);

    al_clear_to_color(al_map_rgb(0,0,0));
    al_draw_text(font, al_map_rgb(255, 255, 255), X_TESTO_HUD, Y_OFFSET_HUD,
                 ALLEGRO_ALIGN_LEFT, "Punteggi migliori:");
    
    for(int i = 0; i < N; i++)
    {
        ALLEGRO_COLOR c = al_map_rgb(255, 255, 255);
        if(h != NULL && hs[i].punteggio == h->punteggio)
            c = al_map_rgb(36, 232, 69);
        char da_scrivere[30] = "";
        sprintf(da_scrivere, "%s  -  %d", hs[i].nome, hs[i].punteggio);
        al_draw_text(font, c, X_TESTO_HUD + 20, Y_INTERLINEA*(i+1) + Y_OFFSET_HUD
                         , ALLEGRO_ALIGN_LEFT, da_scrivere);
    }
    al_flip_display();
    
    al_rest(3.5);
    
    delete[] hs;
    al_destroy_font(font);
    
}