#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_ttf.h>

/** Inizializzazione generale degli addon allegro necessari
 * all'esecuzione del gioco
 */
int inizializza_addon();