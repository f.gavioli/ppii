#include <allegro5/allegro.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_memfile.h>
#include <allegro5/allegro_physfs.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_video.h>

bool inizializza_addon()
{
	if(!al_init())
		return false;
	if(!al_install_keyboard())
		return false;
	if(!al_init_image_addon())
		return false;
	if(!al_init_font_addon())
		return false;
	if(!al_init_ttf_addon())
		return false;
	return true;
}
