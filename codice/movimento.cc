#include "movimento.h"

extern ALLEGRO_BITMAP *DIR_DX;
extern ALLEGRO_BITMAP *DIR_SX;

void aggiorna_velocita_x(dati_gioco &dati)
{
    personaggio &p = dati.personaggi.front();

    //Accelerazione
    if(p.in_movim_dx)
    {
        if(p.vel_x == MAX_VEL_X - 1)
            p.vel_x++;
        else if(p.vel_x < MAX_VEL_X)
            p.vel_x +=2;
        if(p.vel_x > 0)
            return;
    }
    if(p.in_movim_sx)
    {
        if(p.vel_x == MIN_VEL_X + 1)
            p.vel_x--;
        else if(p.vel_x > MIN_VEL_X)
            p.vel_x -= 2;
        if(p.vel_x < 0)
            return;
    }

    //Decelerazione
    if(p.vel_x < 0)
    {
        if(p.vel_x == -1)
            p.vel_x ++;
        else
            p.vel_x += 2;
    }
    else if(p.vel_x > 0)
    {
        if(p.vel_x == 1)
            p.vel_x --;
        else
            p.vel_x -= 2;
    }
}

void aggiorna_velocita_y(dati_gioco &dati)
{
    
}


/*

--------------VECCHIA IA---------------------

void esegui_azione(dati_gioco &dati, personaggio &p)
{
    if(p.tipo == LANCIERE)
    {
        if(p.x < dati.personaggi.front().x)
        {
            p.direzione = DX;
            p.dir_img = DIR_DX;
        }
        else
        {
            p.direzione = SX;
            p.dir_img = DIR_SX;
        }
        spara(dati, p);
    }
    else if(p.tipo == SPADACCINO)
    {
        
    }
}
*/
bool salta(const dati_schermo &schermo, list<oggetto> &oggetti, list<personaggio> &personaggi)
{
    //Movimento
    for (personaggio &p : personaggi)
    {
        if(!p.cade)
        {
            if(p.in_salto)
            {
                if(p.y > MED_ALT_SALTO_1)
                    p.y -= 8;
                else if(p.y > MED_ALT_SALTO_2)
                    p.y -= 6;
                else if(p.y > MED_ALT_SALTO_3)
                    p.y -= 4;
                else if(p.y > MAX_ALT_SALTO)
                    p.y -= 2;
                else
                    p.cade = true;
            }
        }
        else
        {
            if(p.y < MAX_ALT_SALTO)
                p.y += 2;
            else if(p.y < MED_ALT_SALTO_3)
                p.y += 4;
            else if(p.y < MED_ALT_SALTO_2)
                p.y += 6;
            else if(p.y < MED_ALT_SALTO_1)
                p.y += 8;
            else 
            {
                if(p.y + ALT_SPRITE > Y_TERRENO)
                {
                    p.cade = false;
                    p.in_salto = false;
                    p.y = Y_TERRENO - ALT_SPRITE;
                }else
                    p.y += 8;
            }
        }
    }
    
    //Controllo
    bool morto = risolvi_collisioni_pg_pg(personaggi);
    morto = risolvi_collisioni_ogg_pg(oggetti, personaggi) | morto;
    return morto;
}

bool muovi_personaggi(const dati_schermo &schermo, list<personaggio> &personaggi, list<oggetto> oggetti)
{
    //Movimento
    for(personaggio &p : personaggi)
    {
        int diff = schermo.larghezza - (p.x + LARG_SPRITE);
        if(p.x + p.vel_x <= 0)
            p.x = 0;
        else if(p.vel_x > diff)
            p.x += diff;
        else
            p.x += p.vel_x;
        
        if(p.arma.fase_attacco <= MAX_FASI_ATTACCO && p.arma.in_uscita)
        {
            p.arma.y = p.y + OFFSET_ALT_MELEE;
            p.arma.x = p.x + (p.arma.vel_x*p.arma.fase_attacco);
            p.arma.fase_attacco++;
        }
        else if(p.arma.fase_attacco > 0)
        {
            p.arma.y = p.y + OFFSET_ALT_MELEE;
            p.arma.x = p.x + (p.arma.vel_x*p.arma.fase_attacco);
            p.arma.in_uscita = false;
            p.arma.fase_attacco--;
        }
        else
        {
            p.arma.sprite.visibile = false;
            p.in_attacco = false;
        }
    }

    //Controllo
    bool morto = risolvi_collisioni_pg_pg(personaggi);
    morto = risolvi_collisioni_ogg_pg(oggetti, personaggi) | morto;
    return morto;
}

bool muovi_oggetti(const dati_schermo &schermo, list<oggetto> &oggetti, list<personaggio> &personaggi)
{
    //Controllare-Incrementare x, y, se fuori schermo deallocare
    for(oggetto &o : oggetti)
    {
        o.x += o.vel_x;
        o.y += o.vel_y;

        //End of screen Detection
        if(o.x > schermo.larghezza || o.x + LARG_OGG < 0)
        {
            #ifdef DBG 
                cout << "Arma eliminata per fuoriuscita dallo schermo" << endl;
            #endif
            o.sprite.visibile = false;
            o.rim = true;
            assert(o.rim);
            //oggetti.erase(o);
        }
    }

    oggetti.remove_if([](oggetto o){return o.rim;});
    oggetti.remove_if([](oggetto o){return !o.sprite.visibile;});

    //Bullet Hit Detection
    if(oggetti.size() > 1)
        risolvi_collisioni_ogg_ogg(oggetti);
    bool morto = risolvi_collisioni_ogg_pg(oggetti, personaggi);
    return morto;
}