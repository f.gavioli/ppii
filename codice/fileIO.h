#include "dati.h"
#include "statistiche.h"
#include <iostream>

using namespace std;

/** Funzione che carica tutte le immagini
 * di gioco (i nomi indicati nei parametri)
 * dalla cartella img/
 */

bool load_immagini(const char img_sfondo[], const char img_pg[], const char img_col_dx[], 
                    const char img_col_sx[], const char img_lan_dx[], const char img_lan_sx[],
                    const char img_spada_dx[], const char img_spada_sx[], const char img_col_hud[], 
                    const char img_hp_hud[], const char img_npc_hud[], const char img_pausa[], 
                    const char img_dir_dx[], const char img_dir_sx[], const char img_spdc[],
                    const char img_tank[], const char img_insg[]);

/** Funzione che legge i punteggi migliori dal file
  * highscores.sav, e li ritorna in un array di i
  * elementi, parametro di output della funzione.
  */                 
highscore* leggi_highscores(int &i);

/** Funzione che inserisce il punteggio h,
  * passato come parametro, all'interno del
  * file highscores.sav.
  */
bool salva_risultati(highscore h);